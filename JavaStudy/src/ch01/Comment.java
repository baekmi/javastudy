package ch01;

import java.awt.Color;

//파일이름 : Comment.java
//작성자 : 김남현
//작성일 : 2015년 3월
//작성이유 : System.out.println 메소드 기능 테스트
public class Comment {
	static int age = 0;
	/**
	 * 점수에요
	 */
	static int score = 0;
	/**
	 * 주석 사용법
	 */
	public static void main(String[] args) {
		/* 이렇게도 사용 가능 */
		System.out.println(Color.RED);
		System.out.println(Comment.age);
		System.out.println(Comment.score);
	}
}