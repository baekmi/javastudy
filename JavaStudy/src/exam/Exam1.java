package exam;

public class Exam1 {
	public static void main(String[] args) {
		// 코드 작성
		// 아래와 같이 연산 결과를 직접 입력하지 말것!
		// 괄호() 사용
		// Ctrl + Shift + F  자동정렬
		// Ctrl + Shift + C  주석처리 
		
		System.out.println("1 + 2 + 3 의 연산 결과는 '" + (1 + 2 + 3) + "' 입니다");
		System.out.println("1 + 2 + 3 의 연산 결과는 \"" + (1 + 2 + 3) + "\" 입니다");
		System.out.println("1 ~ 5 까지의 곱셈 결과는 " + (1 * 2 * 3 * 4 * 5) + " 입니다");
	}

}
